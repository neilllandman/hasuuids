<?php
/**
 * Created by PhpStorm.
 * User: ranvirmaharaj
 * Date: 2018/05/14
 * Time: 11:15
 */

namespace Landman\HasUuids\Traits;


use Illuminate\Support\Str;

/**
 * Trait ModelUuid
 * Used to generate model uuids
 * @package App\Traits
 */
trait ModelUuid
{

    /**
     * BootTrait
     */
    protected static function bootModelUuid()
    {
        /**
         * Attach to the 'creating' Model Event to provide a UUID
         * for the `id` field (provided by $model->getKeyName()).
         */
        static::creating(function ($model) {
            $model->{$model->getKeyName()} = static::newId();
        });
    }


    /**
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * @return string
     */
    public function getKeyType()
    {
        return 'String';
    }

    protected static function newId(){
        return (string)Str::orderedUuid();
    }
}
